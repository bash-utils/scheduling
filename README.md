# Bash task scheduling utilities
## Maintainer: aachn3 <n45t31@protonmail.com>
## Site: <https://gitlab.com/aachn3/pyutil/rest/api_connector>

### Table of Contents

- [structure](#project-structure)
- [usage](#usage)
- [tests](#testing-library-functionality)
  - [unit](#unit-tests)
  - [integration](#integration-tests)
  

### Project Structure

```
```

### Usage

#### Code samples

#### Testing library functionality

##### Unit tests

Format: None

##### Integration tests

Format: None
