#!/bin/bash

CONFDIR='.config'
PROJECT_NAME_LOWER="$(
    basename "$(
        dirname "$(
            realpath "${BASH_SOURCE[0]}"
        )"
    )" | sed -e 's| \+|_|g' | tr '[:upper:]' '[:lower:]'
)"
PROJECT_NAME_UPPER="$(
    tr '[:lower:]' '[:upper:]' <<< "$PROJECT_NAME_LOWER" | sed -e 's|-|_|g'
)"
CONFIG_DIR="${PROJECT_NAME_UPPER}_CONFIG_DIR"
INCLUDE_DIR="${PROJECT_NAME_UPPER}_INCLUDE_DIR"
INSTALL_DIR="${PROJECT_NAME_UPPER}_INSTALL_DIR"

usage(){
    cat <<-EOF
USAGE: ./install.sh [-h|--help|MODE]
OPTIONS:
    -h|--help               prints this messae and exits
ARGUMENTS:
    MODE        install     attempts to install package using last known config
                clean|uninstall
                            attempts to delete package using last known config
EOF
}

prep_dir(){
    echo -n "mkdir -p "${1}":   "
    if ! [ -d "${1}" ]; then 
        mkdir -p "${1}" && echo "OK" || { echo "FAIL"; return 1; }
    else
        echo "SKIPPED"
    fi
}

install(){
    # import configured paths
    source ${CONFDIR}/FIELDS || { echo "run ./configure first"; exit 4; }

    prep_dir "${!CONFIG_DIR}"     || return 1
    if [[ -d etc ]]; then 
        cp -vr --parents etc/* "${!CONFIG_DIR}" || return 1; 
    fi

    prep_dir "${!INCLUDE_DIR}"    || return 2
    if [[ -d bin ]]; then 
        cp -vr --parents bin/* "${!INCLUDE_DIR}" || return 2
    fi

    prep_dir "${!INSTALL_DIR}"    || return 3
    if [[ -f "${!INCLUDE_DIR}/main.sh" ]]; then
        ln -vs "${!INCLUDE_DIR}/main.sh" \
               "${!INSTALL_DIR}/$PROJECT_NAME_LOWER" || return 3
    elif [[ -f "$PROJECT_NAME_LOWER" ]]; then
        cp -v "$PROJECT_NAME_LOWER" "${!INSTALL_DIR}" || return 3
    else
        return 3
    fi

    cat <<- EOF > "${!INCLUDE_DIR}/.PATHS"
${PROJECT_NAME_UPPER}_SELF="${!INSTALL_DIR}/${PROJECT_NAME_LOWER}"
${CONFIG_DIR}="${!CONFIG_DIR}"
${INCLUDE_DIR}="${!INCLUDE_DIR}"
${INSTALL_DIR}="${!INSTALL_DIR}"
EOF
    echo '$VERSION' > "${!INCLUDE_DIR}/.VERSION"
}

clean(){
    # import configured paths
    source ${CONFDIR}/FIELDS || { echo "run ./configure first"; exit 4; }
    
    rm -ri "${!CONFIG_DIR}"
    rm -ri "${!INCLUDE_DIR}"
    rm -f  "${!INSTALL_DIR}/$PROJECT_NAME_LOWER"
    #rm -ri "${!INSTALL_DIR}"
}


### MAIN ###

# change to working directory
cd "$(dirname "${BASH_SOURCE[0]}")"

case "${1}" in
    ''|install)
        install || { status=$?; clean; exit ${status}; }
        ;;
    clean|uninstall)
        exit $(clean);
        ;;
    -h|--help)
        usage
        exit
        ;;
esac
