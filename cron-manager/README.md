# TITLE

## Maintainer: sample name <sample@domain.com>
## Site: <>
## Version: 

### About


### Table of Contents
- [usage](#usage)
  - [synopsis](#synopsis)
  - [options](#options)
  - [exit codes](#exit-status)
  - [examples](#examples)
- [support](#support)
  - [requirements](#prerequisites)
  - [installation](#installation)
- [description](#description)

### Usage
#### Synopsis
**logging** -h|--version <br/>
**logging** -- \[--logging-nocapture\] \[-qvs|--logging-level=*streams*\] \[--logging-\*-format=*format*\] \[--logging-\*-output=*file*\]

#### Options
###### logging levels
<table border="0">
  <tr>
    <td align="right">-v|--verbose</td>
    <td>outputs lots of useful information</td>
    <td></td>
  </tr>
  <tr>
    <td align="right">-q|--quiet</td>
    <td>default, outputs some information</td>
    <td></td>
  </tr>
  <tr>
    <td align="right">-s|--silent</td>
    <td>completely suppresses console output</td>
    <td></td>
  </tr>
  <tr>
    <td align="right">--logging-level</td>
    <td align="center">streams</td>
    <td>specifies custom logging level from colon separated list of streams</td>
  </tr>
</table>

###### other options
<table border="0">
  <tr>
    <td align="right">-h|--help</td>
    <td>displays help message and exits</td>
  </tr>
  <tr>
    <td align="right">-V|--version</td>
    <td>outputs version string and exits</td>
  </tr>
</table>

#### Exit status
<table border="0">
  <thead>
    <tr>
      <th align="center">NAME</th>
      <th align="center">VALUE</th>
      <th align="center">DESCRIPTION</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="right">EXIT_SUCCESS</td>
      <td align="center">0</td>
      <td>shrepper succeeded</td>
    </tr>
    <tr>
      <td align="right">ERR_ARGVAL</td>
      <td align="center">1</td>
      <td>invalid command-line argument</td>
    </tr>
    <tr>
      <td align="right">ERR_OPFAIL</td>
      <td align="center">*</td>
      <td>amiguous internal error</td>
    </tr>
  </tbody>
</table>

#### Examples

### Support
#### Prerequisites
- bash >= 

#### Installation
`git clone `<br/>
`cd logging`<br/>
`./configure [--install-dir=DIRNAME]`<br/>
`./install.sh`<br/>

### Description
